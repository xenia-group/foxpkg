# foxpkg

foxpkg is a package used to update and install packages on Xenia Linux, where an alternative like flatpak cannot be used. It works by modifying the [catalyst spec files](https://gitlab.com/xenia-group/xenia-linux), and rebuilding the root image on the system.

foxpkg can be used on normal gentoo systems to build roots for Xenia Linux (for example, to build roots on a more powerful system). Roots are usually stored in `/roots`, but this can be changed to `~/.foxpkg/output/` by enabling developer mode.

## Usage

### Global options

-d, --developer: Enables developer mode, which changes the output to `~/.foxpkg/output` and prevents kernel updates from happening on the host.

### foxpkg init

`foxpkg init` will initialise the catalyst repo, symlink the config directory and create a "local" branch in git. This branch is used to store the local changes seperately from the upstream git repo.

### foxpkg update

`foxpkg update` will update the local portage tree, store this in catalyst and then change the dates in the specfiles to reference it.

Specifying -i or --instant will run upgrade after this action.

### foxpkg upgrade

`foxpkg upgrade` will upgrade the root image by re-creating the root image. Can be called with `-f` or `--full` to also re-create the stage1 through stage3 first.

### foxpkg install

> Note: This function does not check if the package is valid or not. Please make sure the package you are adding *actually exists*.

`foxpkg install` will install a new package by adding it to the stage4 spec file and commiting the change.

Specifying -i or --instant will run upgrade after this action.

### foxpkg edit

`foxpkg edit` will open an editor at a file specified in `~/.foxpkg/catalyst/config/stages`, then commit the changes. Useful for editing `package.use` or the like.

Specifying -i or --instant will run upgrade after this action.

### foxpkg revert

`foxpkg revert` will revert the latest commit made to the catalyst repository., then run `upgrade()` This can be used to revert package installations. You can also specify a specific commit to return to. 

Specifying -i or --instant will run upgrade after this action.