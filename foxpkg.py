#!/usr/bin/python

import os, argparse

from datetime import datetime

SPEC_DIR = "/home/luna/.foxpkg/catalyst/"
ROOTS = "/home/luna/Documents/projects/xenia/"


def init(developer):
    os.system(f"git clone https://gitlab.com/xenia-group/catalyst {SPEC_DIR}")
    input("Go to ~/.foxpkg/catalyst now and switch branch if needed, press enter when done: ")
    os.system(f"cd {SPEC_DIR} && git config user.name 'foxpkg' && git config user.email 'foxpkg@example.com'")
    os.system("sudo mkdir -p /var/tmp/catalyst/builds/default/")
    input("Download a stage3 tarball to /var/tmp/catalyst/builds/default/stage3-amd64-openrc-latest.tar.xz, and press enter: ")
    os.system(f"sudo ln -sf {SPEC_DIR}config /var/tmp/catalyst/")
    os.system(f"cd {SPEC_DIR} && git branch local && git checkout local")

    with open(os.path.expanduser(f"{SPEC_DIR}stage4-openrc.spec")) as specfile:
        contents = specfile.read()
    with open(os.path.expanduser(f"{SPEC_DIR}stage4-openrc.spec"), "w") as specfile:
        specfile.write(contents.replace("[CATALYST_DIR]", os.path.expanduser(SPEC_DIR)))
    
    os.system(f"cd {SPEC_DIR} && git add * && git commit -m 'foxpkg: Changed catalyst dir.'")

    update(False, developer)
    upgrade(developer=developer)


def install_root(developer):
    os.system(f"sudo mount -o remount,rw {ROOTS}") if not developer else None
    os.system(f"sudo rm {ROOTS}root_temp.img")
    os.system(f"sudo tar2sqfs {ROOTS}root_temp.img < /var/tmp/catalyst/builds/default/stage4-amd64-openrc-\@TIMESTAMP\@.tar.xz")


def update(instant, developer):
    specfile = open(os.path.expanduser(f"{SPEC_DIR}stage1-openrc.spec")).readlines()

    snapshot_date = [line for line in specfile if line.startswith("snapshot:")][0]
    old_snapshot = snapshot_date.split("snapshot: ")[1].strip()

    os.system("sudo emerge-webrsync")
    date = datetime.today().strftime("%Y.%m.%d")
    os.system(f"sudo catalyst -s {date}")

    for specfilename in [os.path.expanduser(f"{SPEC_DIR}{name}") for name in os.listdir(os.path.expanduser(SPEC_DIR)) if name.endswith(".spec")]:
        with open(specfilename, "r") as specfile:
            contents = specfile.read()
        with open(specfilename, "w") as specfile:
            specfile.write(contents.replace(old_snapshot, date))

    os.system(f"cd {SPEC_DIR} && git add * && git commit -m 'foxpkg: Changed timestamp in specfile.'")

    upgrade(developer=developer) if instant else None


def upgrade_kernel():
    os.system("sudo mkdir /mnt/root")
    os.system("sudo mount /boot")
    os.system(f"sudo mount -o ro,loop -t squashfs {ROOTS}root_temp.img /mnt/root")
    os.system("sudo cp -r /mnt/root/boot/* /boot/")
    os.system("umount /mnt/root")
    os.system(f"sudo rm {ROOTS}root_b.img")
    os.system(f"sudo mv {ROOTS}root.img {ROOTS}root_b.img")
    os.system(f"sudo mv {ROOTS}root_temp.img {ROOTS}root.img")


def upgrade(full=False, stage4=True, developer=False):
    if full:
        os.system(f"sudo catalyst -a -f {SPEC_DIR}stage1-openrc.spec")
        os.system(f"sudo catalyst -a -f {SPEC_DIR}stage3-openrc.spec")

    if stage4:
        os.system(f"sudo catalyst -a -f {SPEC_DIR}stage4-openrc.spec")
        input("Waiting: ")
        install_root(developer)

    upgrade_kernel() if not developer else None


def edit(name, instant, developer):
    os.system(f"nano {SPEC_DIR}config/stages/{name}")
    os.system(f"cd {SPEC_DIR} && git add * && git commit -m 'foxpkg: Edited {name}.'")
    upgrade(developer=developer) if instant else None


def revert(commit=None, instant=False, developer=False):
    os.system(f"cd {SPEC_DIR} && " + ("git reset --hard HEAD~1" if commit == None else f"git reset --hard {commit}"))
    upgrade(developer=developer) if instant else None


def install(package, instant, developer):
    specfile = open(os.path.expanduser(f"{SPEC_DIR}stage4-openrc.spec")).readlines()

    start = specfile[:specfile.index("stage4/packages:\n")]
    packages = specfile[specfile.index("stage4/packages:\n"):specfile.index("stage4/rcadd:\n")]
    end = specfile[specfile.index("stage4/rcadd:\n"):]

    if "/" not in package:
        print("Error: Package not formatted properly.")
    else:
        if package in [pkg.strip("\n").strip("\t") for pkg in packages]:
            print("already in list")
        else:
            packages.append(f"\t{package}\n")

    new = start + packages + end

    with open(os.path.expanduser(f"{SPEC_DIR}stage4-openrc.spec"), "w") as specfile:
        for line in new:
            specfile.write(line)
    
    os.system(f"cd {SPEC_DIR} && git add * && git commit -m 'foxpkg: Added package {package}'")

    upgrade(developer=developer) if instant else None


def parse():
    parser = argparse.ArgumentParser(
        prog = "foxpkg",
        description = "foxpkg enables the installation of packages and updates for Xenia Linux",
        epilog = "Thank you for being apart of Xenia Linux!"
    )

    parser.add_argument("-d", "--developer", action="store_true", help="Enables developer mode: Stores root images in ~/.foxpkg/output and doesn't install kernel")
    
    subparsers = parser.add_subparsers(dest="action", required=True)

    init = subparsers.add_parser("init", help="Initialises the catalyst directory and builds stage1 through stage3")
    
    update = subparsers.add_parser("update", help="Updates the system repositories and updates catalyst repositories")
    update.add_argument("-i", "--instant", action="store_true", help="Upgrades the system after this action.")

    upgrade = subparsers.add_parser("upgrade", help="Upgrades the stage4 to include any package changes or use flag changes.")
    upgrade.add_argument("-f", "--full", action="store_true", help="Also upgrades/rebuilds stage1 and stage3")

    install = subparsers.add_parser("install", help="Adds the specified package to the catalyst spec file.")
    install.add_argument("package")
    install.add_argument("-i", "--instant", action="store_true", help="Upgrades the system after this action.")

    edit = subparsers.add_parser("edit", help="Edits a specific file in the config directory. For example, foxpkg edit package.use/stage4.")
    edit.add_argument("file")
    edit.add_argument("-i", "--instant", action="store_true", help="Upgrades the system after this action.")

    revert = subparsers.add_parser("revert", help="Reverts the last commit, or the commit given after it. Note: This uses git reset, enter the commit you want to go back to.")
    revert.add_argument("commit", nargs="?", default=None)
    revert.add_argument("-i", "--instant", action="store_true", help="Upgrades the system after this action.")

    args = parser.parse_args()
    return args


def main():
    global ROOTS
    arguments = parse()

    if arguments.developer:
        os.system("mkdir /home/luna/.foxpkg/output")
        ROOTS = "/home/luna/.foxpkg/output/"

    match arguments.action:
        case "init":
            init(arguments.developer)
        case "update":
            update(arguments.instant, arguments.developer)
        case "upgrade":
            upgrade(arguments.full, developer=arguments.developer)
        case "install":
            install(arguments.package, arguments.instant, arguments.developer)
        case "edit":
            edit(arguments.file, arguments.instant, arguments.developer)
        case "revert":
            revert(arguments.commit, arguments.instant, arguments.developer)

if __name__ == "__main__":
    main()
